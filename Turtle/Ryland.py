import turtle

def poly(t,x,y,size,side,color):

	t.pencolor('#000000')
	t.fillcolor(color)
	t.begin_fill()
	t.speed(270)
	for n in range (0,4):
		t.forward(size)
		t.left(360/side)
	t.end_fill()

	turtle.update()

def matrix(t,x,y,):
	# color list
	cl = ["#FFFFFF" , "#A10000" ,"#FF1111" ,	"#BC5100" ,"#EB6B09" ,
			"#AF990A" ,	"#FFE11E" ,"#7cafc2" ,"#96609e" ,"#a16946" ]
	# design design list
	mlist = ["0000000000000001100000000",
			 "0000000000000011000000000",
			 "0000000000000011000000000",
			 "0000000000011111000000000",
			 "0000000000111211000000000",
			 "0000000001122311000000000",
			 "0000000001236311000000000",
			 "0000000012346310000000000",
			 "0000000014456410011000000",
			 "0000000124556621001100000",
			 "0000010123556622111100000",
			 "0000110125566652111100000",
			 "0000111125566652134110000",
			 "0000111224566642134210000",
			 "0000122224666642444211000",
			 "0000122343660643466211000",
			 "0000122433600064666221000",
			 "0000112433000006662111000",
			 "0000011246600006643120000",
			 "0000011245666665543200000",
			 "0000001124565533322000000",
			 "0000000012333333210000000",
			 "0000000002222221100000000",
			 "0000000000000000000000000",
			 "0000000000000000000000000"]
	print(t,x,y)
	t.width(1)
	for k in range(0,25):
		for h in range(0,25):
			colorstring = mlist[k][h]
			colorint = int(colorstring)
			color = cl[colorint]
			print(x*20,y*20)
			print(" color ",color," ",end="")
			t.penup()
			t.goto(h*20,k*-20)
			t.pendown()
			poly(t,h,k,20,4,color)
			


def main():
	turtle.TurtleScreen._RUNNING = True
	turtle.screensize(canvwidth=2000, canvheight=2000, bg=None)
	x = -400; y = 0
	w = turtle.Screen()
	w.clear()
	w.bgcolor("#ffffff")
	t = turtle.Turtle()

	turtle.tracer(0, 0)
	
	matrix(t,x,y)
	
	w.exitonclick()
	
if __name__ == '__main__':
	main()

'''
cl = ["#f8f8f8","#e8e8e8" ,"#d8d8d8" ,"#b8b8b8" ,
				"#585858" ,"#383838" ,"#282828" , "#181818" ,
				"#ab4642" ,	"#dc9656" ,"#f79a0e" ,"#538947" ,
				"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]

'''
